<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_transaksi');
            $table->string('hari');
            $table->string('alamat');
            
            $table->string('biaya_pengiriman');
            $table->enum('pengiriman',[0, 1]);
            $table->string('total_produk');
            $table->date('tanggal_pesan');
            
            
            $table->enum('pengiriman_kembali',[0, 1])->nullable();
            $table->string('biaya_pengiriman_kembali')->nullable();
            $table->date('tanggal_pengembalian')->nullable();
            $table->enum('status',[0, 1])->default(0);
            
            $table->string('total_transaksi');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
