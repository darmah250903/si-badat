<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdukTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk_transaksis', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('produk_id')->unsigned()->nullable();
            $table->foreign('produk_id')->references('id')->on('produks')->onDelete('cascade');

            $table->integer('transaksi_id')->unsigned()->nullable();
            $table->foreign('transaksi_id')->references('id')->on('transaksis')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk_transaksis');
    }
}
