<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produks', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('nm_produk');
            $table->string('harga');
            $table->string('kd_produk');
            $table->string('foto');

            $table->integer('catalog_id')->unsigned()->nullable();
            $table->foreign('catalog_id')->references('id')->on('catalogs')->onDelete('cascade');
            
            $table->text('deskripsi');
            $table->integer('stok');
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produks');
    }
}
