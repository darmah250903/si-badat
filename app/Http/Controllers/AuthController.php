<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth,File,Str;
use Validator;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Register a new user
     */
    public function register(Request $request)
    {
        
        $v = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password'  => 'required|min:3',
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }
        // return $request;
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json(['status' => 'success'], 200);
    }


    public function edit(Request $request, $id)
    {
        $user = User::where('id', '=', $id)->first();
        $request = $request->data;

        if(!$user){
            return response()->json(['data' => 'data tidak ada'], 400);
        }

        if($request['foto'] != $request['foto_dir']){
            if(file_exists(public_path().'/images/foto_user/'.$user['foto'])){
                File::delete('images/foto_user/'.$user['foto']);
                // return 'ada';
            }
    
            // return 'tidak ada';
    
            $exploded = explode(',' ,$request['foto']);
            $decoded = base64_decode($exploded[1]);
    
            if(str_contains($exploded[0], 'jpeg'))
                $extension = 'jpg';
    
            else
                $extension = 'png';
    
            $FileName = Str::random(30).'.'.$extension;
            $path = public_path().'/images/foto_user/'.$FileName;
    
            file_put_contents($path, $decoded);
            $request['foto'] = $FileName;
        } else {
            $request['foto'] = $user['foto'];
        }
        



        $user->update([
            'foto'  =>  $request['foto'],
            'name'  =>  $request['name'],
            'email'  =>  $request['email']
        ]);
        return response()->json(['data' => 'sukses Mengupdate '.$id], 200);
        
    }

    /**
     * Login user and return a token
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if ($token = $this->guard()->attempt($credentials)) {
            return response()->json(['status' => 'success'], 200)->header('Authorization', $token);
        }
        return response()->json(['error' => 'login_error'], 401);
    }

    /**
     * Logout User
     */
    public function logout()
    {
        $this->guard()->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }

    /**
     * Get authenticated user
     */
    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }

    /**
     * Refresh JWT token
     */
    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }
        return response()->json(['error' => 'refresh_token_error'], 401);
    }

    /**
     * Return auth guard
     */
    private function guard()
    {
        return Auth::guard();
    }
}