<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use Illuminate\Http\Request;
use App\Models\Produk;
use Illuminate\Support\Str;
use File;

class ProdukController extends Controller
{
    public function index(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            
            // Get User By Search And Per Page
            $produk = Produk::get()->all();

            // Add Columns
            return response()->json($produk);

        }else{
            abort(404);
        }
    }
    public function delete($id)
    {
        $produk = Produk::where('id', '=', $id)->first();
        
        if(file_exists(public_path().'/images/foto_produk/'.$produk['foto'])){
            File::delete('images/foto_produk/'.$produk['foto']);
            // return 'ada';
        }
        if(!$produk){
            return response()->json(['data' => 'data tidak ada'], 400);
        }
        $produk->delete();
        return response()->json(['data' => 'sukses menghapus '.$id], 200);
        
    }

    public function getCatalog()
    {
        $data = Catalog::get();

        if($data){
            return response()->json([
                'data' => $data
            ],200);
        } else {
            return response()->json([
                'message' => 'kesalahan'
            ], 500);
        }
    }

    public function code()
    {
        
        $a = Produk::get('kd_produk');
        $y = array();
        $z = array();
        $c = 0;
        $count = count($a);

        if(!isset($a)){
            return 1;
        }else{
            foreach($a as $value){
                $k = explode('"', $value);
                array_push($y, $k[3]);
            }
            foreach($y as $value){
                $k = explode('-', $value);
                array_push($z, $k[1]);
            }


            sort($z);
                foreach($z as $value){
                    $c++;

                    if(!($c == $value)){
                        return $c;
                        break;
                    }
                }
            return $count+1;
        }
    }

    public function edit(Request $request, $id)
    {
        $produk = Produk::where('id', '=', $id)->first();
        $request = $request->data;

        if(!$produk){
            return response()->json(['data' => 'data tidak ada'], 400);
        }

        if($request['foto'] != $request['foto_dir']){
            if(file_exists(public_path().'/images/foto_produk/'.$produk['foto'])){
                File::delete('images/foto_produk/'.$produk['foto']);
                // return 'ada';
            }
    
            // return 'tidak ada';
    
            $exploded = explode(',' ,$request['foto']);
            $decoded = base64_decode($exploded[1]);
    
            if(str_contains($exploded[0], 'jpeg'))
                $extension = 'jpg';
    
            else
                $extension = 'png';
    
            $FileName = Str::random(30).'.'.$extension;
            $path = public_path().'/images/foto_produk/'.$FileName;
    
            file_put_contents($path, $decoded);
            $request['foto'] = $FileName;
        }
        



        $produk->update([
            'nm_produk'  =>  $request['nm_produk'],
            'kd_produk'  =>  $request['kd_produk'],
            'harga'  =>  $request['harga'],
            'stok'  =>  $request['stok'],
            'foto'  =>  $request['foto'],
            'deskripsi'  =>  $request['deskripsi'],
            'catalog_id'  =>  $request['catalog_id'],
        ]);
        return response()->json(['data' => 'sukses menghapus '.$id], 200);
        
    }

    public function getdata($id)
    {
        // return $id;
        $produk = Produk::where('id', '=', $id)->first();
        if(!$produk){
            return response()->json(['data' => 'data tidak ada'], 400);
        }
        return response()->json(['data' => $produk], 200);
        
    }

    public function create(Request $req)
    {   
        $request = $req->data;
        $exploded = explode(',' ,$request['foto']);
        $decoded = base64_decode($exploded[1]);

        if(str_contains($exploded[0], 'jpeg'))
            $extension = 'jpg';

        else
            $extension = 'png';

        $FileName = Str::random(30).'.'.$extension;
        $path = public_path().'/images/foto_produk/'.$FileName;

        file_put_contents($path, $decoded);
        $request['foto'] = $FileName;

        $table = Produk::create([
            'nm_produk'  =>  $request['nm_produk'],
            'harga'  =>  $request['harga'],
            'kd_produk'  =>  $request['kd_produk'],
            'stok'  =>  $request['stok'],
            'foto'  =>  $request['foto'],
            'deskripsi'  =>  $request['deskripsi'],
            'catalog_id'  =>  $request['catalog_id'],
        ]);

        return 'sukses';
    }
}
