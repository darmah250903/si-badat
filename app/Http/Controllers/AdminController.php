<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            // Set Request Per Page
            $per = (($request->per) ? $request->per : 10);
            
            // Get User By Search And Per Page
            $user = User::where(function($q) use ($request) {
                $q->where('name', 'LIKE', '%'.$request->search.'%')
                ->orWhere('email', 'LIKE', '%'.$request->search.'%');
            })->orderBy('id','asc')->get();

            // Add Columns
            $user->map(function($a) {
                $a->action = '<span class="btn mr-1 btn-sm btn-primary detail" title="Detail" data-id="'.$a->id.'"><i class="bx bx-show"></i></span><span class="btn mr-1 btn-sm btn-danger delete" title="Hapus" data-id="'.$a->id.'"><i class="bx bx-trash"></i></span><span class="btn mr-1 btn-sm btn-warning edit" title="Edit" data-id="'.$a->id.'"><i class="bx bx-pencil"></i></span>';
                return $a;
            });
            return response()->json($user);

        }else{
            abort(404);
        }
    }
    public function delete($id)
    {
        $user = User::where('id', '=', $id)->first();
        if(!$user){
            return response()->json(['data' => 'data tidak ada'], 400);
        }
        $user->delete();
        return response()->json(['data' => 'sukses menghapus '.$id], 200);
        
    }

    public function edit(Request $request, $id)
    {
        $user = User::where('id', '=', $id)->first();
        if(!$user){
            return response()->json(['data' => 'data tidak ada'], 400);
        }
        $user->update($request['data']);
        return response()->json(['data' => 'sukses menghapus '.$id], 200);
        
    }

    public function getdata($id)
    {
        $user = User::where('id', '=', $id)->first();
        if(!$user){
            return response()->json(['data' => 'data tidak ada'], 400);
        }
        return response()->json(['data' => $user], 200);
        
    }
}
