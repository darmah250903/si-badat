<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Carousel;
use App\Models\Keranjang;
use App\Models\Produk;
use App\Models\Produk_transaksi;
use App\Models\Transaksi;

class UserController extends Controller
{

    public function bayarkembali(Request $request)
    {
        $data = Transaksi::with('barang')->where('id', '=', $request->data['id'])->first();
        $data->update([
            'status' => '1',
            'pengiriman_kembali' => $request->data['pengiriman_kembali'],
            'biaya_pengiriman_kembali' => $request->data['biaya_pengiriman_kembali'],
            'total_transaksi' => $request->data['total_transaksi'],
            'tanggal_pengembalian' => date('Y-m-d')
        ]);

        if($data){
            foreach ($data['barang'] as $item) {
                $produk = Produk::where('id', '=', $item['id'])->first();
                $produk->update([
                    'stok' => intval($produk['stok']) + 1
                ]);
            }

            return response()->json([
                'pesan' => 'sukses transaski'
            ], 200);
            
        } else {
            return response()->json([
                'pesan' => 'gagal'
            ], 500);
        }
    }

    public function getkembali($id)
    {
        $data = Transaksi::with('barang')->where([['user_id', '=', $id], ['status', '=', '0']])->get();
        return response()->json([
            'data' => $data
        ],200);
    }

    public function getall()
    {
        $caraousel = Carousel::get()->all();
        $produk = Produk::where('stok', '>', 0)->get()->all();

        return Response()->Json([
            'carousel' => $caraousel,
            'produk' => $produk
        ]);
    }

    public function transaksi(Request $request)
    {

        
        $tra = Transaksi::orderBy('id', 'desc')->first();

        if(!$tra){
            $request['kode_transaksi'] = str_pad(0, 20, '0', STR_PAD_LEFT);
        } else {
            $request['kode_transaksi'] = str_pad($tra->id + 1, 20, '0', STR_PAD_LEFT);
        }


        $data = Transaksi::create([
            'alamat' => $request->data['alamat'],
            'kode_transaksi' => $request['kode_transaksi'],
            'hari' => $request->data['harifix'],
            'total_produk' => $request->data['jumlah_produk'],
            'total_transaksi' => $request->data['jumlahfix'],
            'pengiriman' => $request->data['pengiriman'],
            'biaya_pengiriman' => $request->data['jumlahpengiriman'],
            'user_id' => $request->data['id'],
            'tanggal_pesan' => date('Y-m-d')
        ]);
        
        if($data){
            foreach ($request->data['keranjang'] as $item) {
                Produk_transaksi::create([
                    'produk_id' => $item['produk_id'],
                    'transaksi_id' => $data['id']
                ]);

                $produk = Produk::where('id', '=', $item['produk_id'])->first();
                $produk->update([
                    'stok' => intval($produk['stok']) - 1
                ]);
            }

            Keranjang::where('user_id','=', $request->data['id'])->delete();

            return response()->json([
                'pesan' => 'sukses transaski'
            ], 200);
        } else {
            return response()->json([
                'pesan' => 'Gagal Transaksi'
            ], 500);
        }

        
        

    }

    public function addkeranjang(Request $request)
    {

        $oioi = Keranjang::where('produk_id', '=', $request->produk_id)->first();
        if(!empty($oioi)){
            return Response()->Json([
                'pesan' => 'error'
            ], 400);
        }

        $data = Keranjang::create([
            'produk_id'=>$request->produk_id,
            'user_id'=>$request->user_id,
        ]);
        if(!$data){
            return Response()->Json([
                'pesan' => 'error'
            ], 400);
        }
        return Response()->Json([
            'pesan' => 'sukses'
        ],200);
    }

    public function getkeranjang($id)
    {
        $data = Keranjang::where('user_id', '=', $id)->with(['produk'])->get();
        return $data;
    }

    public function deletekeranjang($id)
    {
        $data = Keranjang::where('id', '=', $id)->first();
        
        if(!$data){
            return Response()->Json([
                'pesan' => 'tidak data'
            ]);
        }
        $data->delete();

        return Response()->Json([
            'pesan' => 'berhasil menghapus data'
        ]);
    }
}
