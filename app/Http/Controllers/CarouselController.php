<?php

namespace App\Http\Controllers;

use App\Models\Carousel;
use Illuminate\Http\Request;
use File, Str;

class CarouselController extends Controller
{
    public function index(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            
            // Get User By Search And Per Page
            $carousel = Carousel::get()->all();

            // Add Columns
            return response()->json($carousel);

        }else{
            abort(404);
        }
    }
    public function delete($id)
    {
        $carousel = Carousel::where('id', '=', $id)->first();
        
        if(file_exists(public_path().'/images/carousel/'.$carousel['foto'])){
            File::delete('images/carousel/'.$carousel['foto']);
            // return 'ada';
        }
        if(!$carousel){
            return response()->json(['data' => 'data tidak ada'], 400);
        }
        $carousel->delete();
        return response()->json(['data' => 'sukses menghapus '.$id], 200);
        
    }


    public function edit(Request $request, $id)
    {
        $carousel = Carousel::where('id', '=', $id)->first();
        $request = $request->data;

        if(!$carousel){
            return response()->json(['data' => 'data tidak ada'], 400);
        }

        if($request['foto'] != $request['foto_dir']){
            if(file_exists(public_path().'/images/carousel/'.$carousel['foto'])){
                File::delete('images/carousel/'.$carousel['foto']);
                // return 'ada';
            }
    
            // return 'tidak ada';
    
            $exploded = explode(',' ,$request['foto']);
            $decoded = base64_decode($exploded[1]);
    
            if(str_contains($exploded[0], 'jpeg'))
                $extension = 'jpg';
    
            else
                $extension = 'png';
    
            $FileName = Str::random(30).'.'.$extension;
            $path = public_path().'/images/carousel/'.$FileName;
    
            file_put_contents($path, $decoded);
            $request['foto'] = $FileName;
        }
        



        $carousel->update([
            'foto'  =>  $request['foto']
        ]);
        return response()->json(['data' => 'sukses Mengupdate '.$id], 200);
        
    }

    public function getdata($id)
    {
        // return $id;
        $carousel = Carousel::where('id', '=', $id)->first();
        if(!$carousel){
            return response()->json(['data' => 'data tidak ada'], 400);
        }
        return response()->json(['data' => $carousel], 200);
        
    }

    public function create(Request $req)
    {   
        $request = $req->data;
        $exploded = explode(',' ,$request['foto']);
        $decoded = base64_decode($exploded[1]);

        if(str_contains($exploded[0], 'jpeg'))
            $extension = 'jpg';

        else
            $extension = 'png';

        $FileName = Str::random(30).'.'.$extension;
        $path = public_path().'/images/carousel/'.$FileName;

        file_put_contents($path, $decoded);
        $request['foto'] = $FileName;

        $table = Carousel::create([
            'foto'  =>  $request['foto'],
        ]);

        return 'sukses';
    }
}
