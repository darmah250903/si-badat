<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use Illuminate\Http\Request;
use Validator;

class CatalogController extends Controller
{
    public function index(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            // Set Request Per Page
            $per = (($request->per) ? $request->per : 10);
            
            // Get User By Search And Per Page
            $user = Catalog::where(function($q) use ($request) {
                $q->where('nama', 'LIKE', '%'.$request->search.'%');
            })->orderBy('id','asc')->get();

            // Add Columns
            $user->map(function($a) {
                $a->action = '<span class="btn mr-1 btn-sm btn-danger delete" title="Hapus" data-id="'.$a->id.'"><i class="bx bx-trash"></i></span><span class="btn mr-1 btn-sm btn-warning edit" title="Edit" data-id="'.$a->id.'"><i class="bx bx-pencil"></i></span>';
                return $a;
            });
            return response()->json($user);

        }else{
            abort(404);
        }
    }
    public function delete($id)
    {
        $user = Catalog::where('id', '=', $id)->first();
        if(!$user){
            return response()->json(['data' => 'data tidak ada'], 400);
        }
        $user->delete();
        return response()->json(['data' => 'sukses menghapus '.$id], 200);
        
    }

    public function Tambah(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $validator = Validator::make($request['data'], [
                'nama'  => 'required|string',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status'    => false,
                    'message'   => $validator->messages()->first()
                ], 400);
            }
        }

        $data = Catalog::create($request['data']);

        if($data){
            return response()->json([
                'pesan' => 'sukses menambah data'
            ], 200);
        } else {
            return response()->json([
                'pesan' => 'gagal menambah data'
            ], 400);
            
        }
    }

    public function edit(Request $request, $id)
    {
        $user = Catalog::where('id', '=', $id)->first();
        if(!$user){
            return response()->json(['data' => 'data tidak ada'], 400);
        }
        $user->update($request['data']);
        return response()->json(['data' => 'sukses menghapus '.$id], 200);
        
    }

    public function getdata($id)
    {
        $user = Catalog::where('id', '=', $id)->first();
        if(!$user){
            return response()->json(['data' => 'data tidak ada'], 400);
        }
        return response()->json(['data' => $user], 200);
        
    }
}
