<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $fillable = [
        'nm_produk', 'kd_produk', 'foto', 'deskripsi', 'stok', 'harga', 'catalog_id'
    ];
    protected $appends = [
        'foto_dir'
    ];
    
    function getFotoDirAttribute(){
        return asset('images/foto_produk/'.$this->foto);
    }

    public function catalog()
    {
        return $this->hasOne(Catalog::class, 'id', 'catalog_id');
    }
}



