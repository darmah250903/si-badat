<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $fillable = [
        'total_transaksi','total_produk', 'kode_transaksi', 'user_id', 'alamat', 'hari', 'biaya_pengiriman', 'pengiriman', 'pengiriman_kembali', 'biaya_pengiriman_kembali', 'status', 'tanggal_pesan', 'tanggal_pengembalian'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function barang()
    {

    return $this->belongsToMany(Produk::class, 'produk_transaksis', 'transaksi_id', 'produk_id');
    }

}
