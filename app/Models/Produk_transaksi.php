<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produk_transaksi extends Model
{
    protected $fillable = [
        'produk_id', 'transaksi_id'
    ];
}
