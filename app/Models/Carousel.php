<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carousel extends Model
{
    
    protected $fillable = ['foto'];

    protected $appends = [
        'foto_dir'
    ];
    
    function getFotoDirAttribute(){
        return asset('images/carousel/'.$this->foto);
    }
}
