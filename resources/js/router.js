import VueRouter from 'vue-router'
// Pages
// import Home from './pages/Home'
// import About from './pages/About'
import Redirect from './pages/redirect'
import Login from './pages/Login'
import Dashboard from './pages/user/Dashboard'
// import AdminDashboard from './pages/admin/Dashboard'

import Admin from './routes/Admin';
import User from './routes/User';




// Routes
const routes = [
  {
    path: '/',
    name: 'home',
    component: Redirect,
    meta: {
      title: 'loading....',
      auth: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      title: 'login',
      auth: false
    }
  },
  // USER ROUTES
];

var $routes = routes.concat(Admin);
$routes = $routes.concat(User);

const notfound = [
  {
      name: 'notfound',
      path: '/*',
      component: () => import(/* webpackChunkName: "main" */ './pages/error/NotFoundRedirect'),
      meta: {
          title: `Halaman tidak ditemukan`,
          auth: undefined
      }
  },
  {
      name: 'notfound404',
      path: '/404',
      component: () => import(/* webpackChunkName: "main" */ './pages/error/NotFound'),
      meta: {
          title: `Halaman tidak ditemukan`,
          auth: undefined
      }
  }
];

$routes = $routes.concat(notfound);



const router = new VueRouter({
  history: true,
  mode: 'history',
  routes: $routes,
  linkExactActiveClass: "active"
});

router.beforeEach((to, from, next) => {
  var title = document.querySelector("meta[name='title']").getAttribute("content");
  document.title = to.meta.title
  next();});


export default router