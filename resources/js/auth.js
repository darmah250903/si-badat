
import driverAuthBearer from '@websanova/vue-auth/dist/drivers/auth/bearer.js';
import driverHttpAxios from '@websanova/vue-auth/dist/drivers/http/axios.1.x.js';
import driverRouterVueRouter from '@websanova/vue-auth/dist/drivers/router/vue-router.2.x.js';

const config = {
  auth: driverAuthBearer,
  http: driverHttpAxios,
  router: driverRouterVueRouter,
  tokenDefaultName: 'laravel-vue-spa',
  tokenStore: ['localStorage'],
  rolesVar: 'role',
  registerData: { url: 'auth/register', method: 'POST', redirect: '/login' },
  loginData: { url: 'auth/login', method: 'POST', redirect: '', fetchUser: true },
  logoutData: { url: 'auth/logout', method: 'POST', redirect: '/', makeRequest: true },
  fetchData: { url: 'auth/user', method: 'GET', enabled: true },
  refreshData: { url: 'auth/refresh', method: 'GET', enabled: true, interval: 30 }
}
export default config