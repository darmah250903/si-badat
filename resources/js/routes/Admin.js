const Admin = [
    // START ADMIN //
    {
        name: 'admin',
        path: '/admin',
        component: () => import(/* webpackChunkName: "admin" */ '../pages/admin/Dashboard'),
        meta: {
            title: `Dashboard`,
            auth: true,
            breadcrumb: ['Admin',  'Dashboard']
        }
    },
    {
        name: 'admin.produk',
        path: '/admin/produk',
        component: () => import(/* webpackChunkName: "admin" */ '../pages/admin/Produk'),
        meta: {
            title: `Produk`,
            auth: true,
            breadcrumb: ['Admin',  'Dashboard']
        }
    },
    {
        name: 'admin.carousel',
        path: '/admin/carousel',
        component: () => import(/* webpackChunkName: "admin" */ '../pages/admin/Carousel'),
        meta: {
            title: `Produk`,
            auth: true,
            breadcrumb: ['Admin',  'Dashboard']
        }
    },
    {
        name: 'admin.catalog',
        path: '/admin/catalog',
        component: () => import(/* webpackChunkName: "admin" */ '../pages/admin/Catalog'),
        meta: {
            title: `Produk`,
            auth: true,
            breadcrumb: ['Admin',  'Dashboard']
        }
    },
    // {
    //     name: 'admin.carousel',
    //     path: '/admin/carousel  ',
    //     component: () => import(/* webpackChunkName: "admin" */ '../pages/admin/Carousel'),
    //     meta: {
    //         title: `Carousel`,
    //         auth: true,
    //         breadcrumb: ['Admin',  'Dashboard']
    //     }
    // },
    // {
    //     name: 'admin.setting',
    //     path: '/admin/setting',
    //     component: () => import(/* webpackChunkName: "admin.setting" */ '../pages/admin/setting/Index'),
    //     meta: {
    //         title: `Setting`,
    //         auth: true,
    //         breadcrumb: ['Admin', 'Setting']
    //     }
    // },
    // END ADMIN //
];

export default Admin