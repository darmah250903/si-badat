const User = [
    // START USER //
    {
        name: 'user',
        path: '/user',
        component: () => import(/* webpackChunkName: "user" */ '../pages/user/Dashboard'),
        meta: {
            title: `Dashboard`,
            auth: true,
            breadcrumb: ['User',  'Dashboard']
        }
    },
    {
        name: 'user.edit',
        path: '/user/edit',
        component: () => import(/* webpackChunkName: "user" */ '../pages/User'),
        meta: {
            title: `Edit User`,
            auth: true,
            breadcrumb: ['User',  'Edit User']
        }
    },
    {
        name: 'user.keranjang',
        path: '/user/keranjang',
        component: () => import(/* webpackChunkName: "user" */ '../pages/user/Keranjang'),
        meta: {
            title: `keranjang User`,
            auth: true,
            breadcrumb: ['User',  'Keranjang User']
        }
    },
    {
        name: 'user.kembali',
        path: '/user/kembali',
        component: () => import(/* webpackChunkName: "user" */ '../pages/user/Kembali'),
        meta: {
            title: `Kembali User`,
            auth: true,
            breadcrumb: ['User',  'Keranjang User']
        }
    },
    // END USER //
];

export default User