import 'es6-promise/auto'
import axios from 'axios'
import './bootstrap'
import Vue from 'vue'
import jQuery from 'jquery';
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import Index from './App.vue'
import router from './router'

import 'toastr/build/toastr.css';
import 'boxicons/css/boxicons.min.css';

import'select2/dist/css/select2.min.css'
import select2 from 'select2';

import Swal from 'sweetalert2/dist/sweetalert2.js'

window.Swal = Swal;
window.select2 = select2;

import 'sweetalert2/src/sweetalert2.scss'

import auth from '@websanova/vue-auth/dist/v2/vue-auth.common.js';
import driverAuthBearer from '@websanova/vue-auth/dist/drivers/auth/bearer.js';
import driverHttpAxios from '@websanova/vue-auth/dist/drivers/http/axios.1.x.js';
// import driverHttpVueResource from '@websanova/vue-auth/dist/drivers/http/vue-resource.1.x.js';
import driverRouterVueRouter from '@websanova/vue-auth/dist/drivers/router/vue-router.2.x.js';


window.Vue = Vue
// Set Vue router
Vue.router = router
Vue.use(VueRouter)
// Set Vue authentication
Vue.use(VueAxios, axios)
axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api/v1`;

import toastr from "toastr";
window.toastr = toastr;

$ = jQuery;

Vue.use(auth, {
  plugins: {
    http: Vue.axios, // Axios
    // http: Vue.http, // Vue Resource
    router: Vue.router,
  },
  drivers: {
    auth: driverAuthBearer,
    http: driverHttpAxios, // Axios
    // http: driverHttpVueResource, // Vue Resource
    router: driverRouterVueRouter,
    tokenDefaultName: 'laravel-vue-spa',
    tokenStore: ['localStorage'],
    rolesVar: 'role',
    registerData: { url: 'auth/register', method: 'POST', redirect: '/login' },
    loginData: { url: 'auth/login', method: 'POST', redirect: '', fetchUser: true },
    logoutData: { url: 'auth/logout', method: 'POST', redirect: '/', makeRequest: true },
    fetchData: { url: 'auth/user', method: 'GET', enabled: true },
    refreshData: { url: 'auth/refresh', method: 'GET', enabled: true, interval: 30 }
  },
});





// Set Vue globally



// Load Index
Vue.component('index', Index)
const app = new Vue({
  el: '#app',
  router
});