<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function () {
    Route::prefix('auth')->group(function () {

        // Below mention routes are public, user can access those without any restriction.
        // Create New User
        Route::post('register', 'AuthController@register');

        // Login User
        Route::post('login', 'AuthController@login');
        
        // Refresh the JWT Token
        Route::get('refresh', 'AuthController@refresh');
        
        // Below mention routes are available only for the authenticated users.
        
        Route::middleware('auth:api')->group(function () {
            // Get user info
            Route::get('user', 'AuthController@user');
            Route::prefix('user')->group(function () {
                Route::get('getall', 'UserController@getall');
                Route::get('{id}/getkeranjang', 'UserController@getkeranjang');
                Route::get('{id}/deletekeranjang', 'UserController@deletekeranjang');
                Route::get('{id}/getkembali', 'UserController@getkembali');
                Route::post('kembali/bayar', 'UserController@bayarkembali');
                Route::post('keranjang', 'UserController@addkeranjang');
                Route::post('checkout', 'UserController@transaksi');
                Route::post('edit/{id}', 'AuthController@edit');

            });
            Route::prefix('admin')->group(function () {
                
                Route::post('index', 'AdminController@index');
                Route::get('{id}/delete', 'AdminController@delete');
                Route::post('{id}/edit', 'AdminController@edit');
                Route::get('{id}/getdata', 'AdminController@getdata');
                Route::prefix('produk')->group(function () {
                    Route::get('index', 'ProdukController@index');
                    Route::get('{id}/delete', 'ProdukController@delete');
                    Route::post('{id}/edit', 'ProdukController@edit');
                    Route::post('create', 'ProdukController@create');
                    Route::get('{id}/getdata', 'ProdukController@getdata');
                    Route::get('getcode', 'ProdukController@code');
                    Route::get('getCatalog', 'ProdukController@getCatalog');

                });
                Route::prefix('catalog')->group(function () {
                    Route::post('index', 'CatalogController@index');
                    Route::post('create', 'CatalogController@tambah');
                    Route::get('{id}/delete', 'CatalogController@delete');
                    Route::post('{id}/edit', 'CatalogController@edit');
                    Route::get('{id}/getdata', 'CatalogController@getdata');

                });
                Route::prefix('carousel')->group(function () {
                    Route::get('index', 'CarouselController@index');
                    Route::get('{id}/delete', 'CarouselController@delete');
                    Route::post('{id}/edit', 'CarouselController@edit');
                    Route::post('create', 'CarouselController@create');
                    Route::get('{id}/getdata', 'CarouselController@getdata');

                });
                
            });
            // Logout user from application
            Route::post('logout', 'AuthController@logout');
        });
    });
});
